export default {
    name:"List",
    data: () => ({
        selection : [],
    }),
    methods:{
        clickMenu(item){
            this.$store.commit("updateMenuSelection", item);
        }
    },
    props: {
        title:{
            type : String,
            default: "Titre"
        },
        items:{
            type : Array,
            default : function(){ return []}
        }
    },
    mounted(){
        this.$store.commit("updateMenuSelection", null);
    },
    computed:{

    }
    
  }