import LineChart from "../../../components/LineChart/LineChart";
import PulseLoader from "vue-spinner/src/PulseLoader";
import Filtering from '../../../components/filtering/filtering';

export default {
    name: "yearSummary",
    components: {
        LineChart,
        PulseLoader,
        Filtering
    },
    props: ['year'],
    data: function () {
        return {
            selected: ["Cleaness", "Satisfaction", "Frequenting"],
            apiPath: this.$store.state.apiAddress + 'stations/stats/' + this.year,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        type: 'category',
                        labels: []
                    }]
                }
            },
            filter: {
                values: ["Propreté", "Satisfaction", "Fréquentation", "Nom de station"], name: "Trier par", selected: "Satisfaction"
            },
        }
    },
    computed: {
        currentChart() {
            if (this.$store.state.apiProxy[this.apiPath]) {
                return this.formatDataForChart(this.$store.state.apiProxy[this.apiPath], this.selected);
            } else {
                this.$store.dispatch('fetchApi', this.apiPath);
            }
            return this.$store.state.apiProxyChangeTracker && [];
        },
        orderBy() {
          switch(this.filter.selected) {
              case "Propreté":
                  return "CLE_CLEANESS";
              case "Satisfaction":
                  return "SATI_VALUE";
              case "Fréquentation":
                  return "FREQ_VALUE";
              case "Nom de station":
                  return "STA_LIBELLE";
              default:
                  return null;
          }
        },
        loading() {
            return this.$store.state.loading;
        }
    },
    methods: {
        formatDataForChart: function (apiData, selected) {
            let localData = apiData.sort((a, b) => {
                if (a[this.orderBy] < b[this.orderBy]) {
                    return -1;
                } else if (a[this.orderBy] === b[this.orderBy]) {
                    return 0;
                }
                return 1;
            });
            let satisfactionData = [];
            let cleanessData = [];
            let freqData = [];
            let labels = [];
            for (let station of localData) {
                satisfactionData.push(station["SATI_VALUE"]);
                cleanessData.push(station["CLE_CLEANESS"]);
                freqData.push(Math.log(station["FREQ_VALUE"]));
                labels.push(station["STA_LIBELLE"]);
            }
            this.options.scales.xAxes[0].labels = labels;
            let chart = {datasets: []};
            let cleaness = {
                label: "Propreté",
                data: cleanessData,
                fill: false,
                backgroundColor: "#ff9e05",
                borderColor: "#ff9e05",
                borderWidth: 1
            };
            let satisfaction = {
                label: "Satisfaction",
                data: satisfactionData,
                fill: false,
                backgroundColor: "#444bff",
                borderColor: "#444bff",
                borderWidth: 1
            };
            let freq = {
                label: "Fréquentation",
                data: freqData,
                fill: false,
                backgroundColor: "#ff00c9",
                borderColor: "#ff00c9",
                borderWidth: 1
            };
            if (selected.indexOf("Cleaness") !== -1){
                chart["datasets"].push(cleaness);
            }
            if (selected.indexOf("Satisfaction") !== -1){
                chart["datasets"].push(satisfaction);
            }
            if (selected.indexOf("Frequenting") !== -1){
                chart["datasets"].push(freq);
            }
            return chart;
        },
    },
    watch: {
        selected: function (val) {
        }
    }
}