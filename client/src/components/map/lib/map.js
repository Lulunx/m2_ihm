import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet.markercluster';
import 'leaflet.markercluster/dist/MarkerCluster.css';
import 'leaflet.markercluster/dist/MarkerCluster.Default.css';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

export default {
    data() {
        return {
            map: null,
            markers: null,
            redIcon: new L.Icon({
                iconUrl: require('@/assets/leaflet-icons/marker-icon-2x-red.png'),
                shadowUrl: require('@/assets/leaflet-icons/marker-shadow.png'),
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            }),
            greenIcon: new L.Icon({
                iconUrl: require('@/assets/leaflet-icons/marker-icon-2x-green.png'),
                shadowUrl: require('@/assets/leaflet-icons/marker-shadow.png'),
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            }),
            orangeIcon: new L.Icon({
                iconUrl: require('@/assets/leaflet-icons/marker-icon-2x-orange.png'),
                shadowUrl: require('@/assets/leaflet-icons/marker-shadow.png'),
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            })
        }
    },
    methods: {
        addToMap(newPointer) {


            if (newPointer.STA_COORD_X != null && newPointer.STA_COORD_Y != null) {
                switch (this.selection.api) {

                    case "stations":
                        this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.greenIcon }).bindPopup(newPointer.text));
                        break;

                    case "cleanesses":
                        let pourcentage = 1 - (newPointer.CLE_IMPROPER / newPointer.CLE_OBSERVATIONS)
                        if (pourcentage >= 0.95) {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.greenIcon }).bindPopup(newPointer.text));
                        }
                        else if (pourcentage >= 0.90) {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.orangeIcon }).bindPopup(newPointer.text));
                        }
                        else {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.redIcon }).bindPopup(newPointer.text));
                        }
                        break;

                    case "frequentings":
                        if (newPointer.FREQ_VALUE < 100000) {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.greenIcon }).bindPopup(newPointer.text));
                        }
                        else if (newPointer.FREQ_VALUE < 1000000) {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.orangeIcon }).bindPopup(newPointer.text));
                        }
                        else {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.redIcon }).bindPopup(newPointer.text));
                        }
                        break;

                    case "satisfactions":
                        if (newPointer.SATI_VALUE > 7.75) {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.greenIcon }).bindPopup(newPointer.text));
                        }
                        else if (newPointer.SATI_VALUE > 7.5) {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.orangeIcon }).bindPopup(newPointer.text));
                        }
                        else {
                            this.markers.addLayer(L.marker([newPointer.STA_COORD_X, newPointer.STA_COORD_Y], { icon: this.redIcon }).bindPopup(newPointer.text));
                        }
                        break;
                }
            }
        },
    },
    mounted() {
        this.markers = L.markerClusterGroup({
            maxClusterRadius: 50,
            iconCreateFunction: function (cluster) {
                return new L.DivIcon({
                    html: '<b>' + cluster.getChildCount() + '</b>',
                    className: 'marker-cluster-test', iconSize: new L.Point(40, 40)
                });
            }
        });
        this.map = L.map('map', {});
        this.map.setView([47.1, 3], 6);
        L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attributions">CARTO</a>',
            subdomains: 'abcd',
            maxZoom: 20,
            minZoom: 0
        }).addTo(this.map);
        this.markers.addTo(this.map);


    },
    computed:{
        legends(){
            if(this.selection)
            {
                return this.selection.legends;
            }
            return null;
        }
    },
    props: {
        dataAPI: {
            type: Array,
            default: function () { return [] }
        },
        selection: {
            type: Object,
            default: function () { return {} }
        }
    },
    watch: {
        dataAPI: function (newVal) {
            this.markers.clearLayers();
            switch (this.selection.api) {
                case "stations":
                    for (var i = 0; i < newVal.length; i++) {
                        if (!newVal[i].text) {
                            newVal[i].text = "Gare : " + newVal[i].STA_LIBELLE;
                        }
                    }
                    break;
                case "cleanesses":
                    for (var i = 0; i < newVal.length; i++) {
                        if (!newVal[i].text) {
                            newVal[i].text =
                                "Gare : " + newVal[i].STA_LIBELLE +
                                "<br>Propreté : " + Math.round(((1 - (newVal[i].CLE_IMPROPER / newVal[i].CLE_OBSERVATIONS)) * 10) * 100) / 100 + "/10" +
                                "<br>Nb Contrôles : " + newVal[i].CLE_OBSERVATIONS +
                                "<br>Nb Sale : " + newVal[i].CLE_IMPROPER;
                        }
                    }
                    break;
                case "frequentings":
                    for (var i = 0; i < newVal.length; i++) {
                        if (!newVal[i].text) {
                            newVal[i].text =
                                "Gare : " + newVal[i].STA_LIBELLE +
                                "<br>Fréquentation : " + Math.floor(newVal[i].FREQ_VALUE);
                        }
                    }
                    break;
                case "satisfactions":
                    for (var i = 0; i < newVal.length; i++) {
                        if (!newVal[i].text) {
                            newVal[i].text =
                                "Gare : " + newVal[i].STA_LIBELLE +
                                "<br>Satisfaction : " + (Math.round((newVal[i].SATI_VALUE * 100)) / 100);
                        }
                    }
                    break;
            }

            for (var i = 0; i < newVal.length; i++) {
                this.addToMap(newVal[i]);
            }

        },
    }
}


