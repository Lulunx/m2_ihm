export default {
    name: "ListCheck",
    data: () => ({
        selected: []
    }),
    methods: {
        uncheckAll: function() {
            this.selected = [];
        }
    },
    props: {
        title: {
            type: String,
            default: "Titre"
        },
        items: {
            type: Array,
            default: function () {
                return []
            }
        }
    },
    mounted() {
        this.$store.commit("updateMenuListSelection", null);
    },
    computed: {},
    watch: {
        selected() {
            this.$store.commit("updateMenuListSelection", this.selected);
        }
    }

}