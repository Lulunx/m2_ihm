import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        apiProxyChangeTracker: {
            text: 0
        },
        apiProxy: new Map(),
        apiAddress: "http://185.229.179.183:8077/",
        loading: false,

        snackMessage: '',
        snackShow: false,
        snackColor: "success",

        menuSelection: null,
        menuListSelection: [],
    },
    getters: {
        status: state => state.status
    }
    ,
    mutations: {
        updateSnackMessage: (state, msg) => {
            state.snackMessage = msg;
        },

        showSnack: (state, status) => {
            state.snackShow = status;
        },

        setSnackColor: (state, color) => {
            state.snackColor = color;
        },

        updateMenuSelection: (state, selection) => {
            state.loading = false;
            state.menuSelection = selection;
        },

        updateMenuListSelection: (state, selection) => {
            state.loading = false;
            state.menuListSelection = selection;
        },

        updateLoading: (state, value) => {
            state.loading = value;
        },
        updateApiProxy: (state, dict) => {
            Vue.set(state.apiProxy, dict["req"], dict["content"]);
            state.apiProxyChangeTracker += 1;
        },
    },
    actions: {
        fetchApi: (store, req) => {
            store.commit('updateLoading', true);
            try {
                if (!store.state.apiProxy[req]) {
                    fetch(req)
                        .then(res => res.json()
                            .then(res => {
                                store.commit('updateApiProxy', {'req': req, 'content': res.content});
                                store.commit('updateLoading', false);
                            }));
                }
            } catch (e) {
                console.error(e);
                store.commit('updateLoading', false);
            }
        }
    }
})
