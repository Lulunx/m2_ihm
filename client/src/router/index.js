import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home/Home.vue'
import About from '../views/About/About.vue'
import TravelReasonOverview from '../views/TravelReasonOverview/TravelReasonOverview'
import YearlySummariesView from '../views/YearlySummaries/YearlySummariesView'
import DiagramView from '../views/DiagramView/DiagramView'
import MapVisualisation from '../views/MapVisualisation/MapVisualisation.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/travelreason',
    name: 'TravelReasonOverview',
    component: TravelReasonOverview
  }, {
    path: '/yearlySummaries',
    name: 'YearlySummariesView',
    component: YearlySummariesView
  },
  {
    path: '/MapVisualisation',
    name: 'MapVisualisation',
    component: MapVisualisation
  },
  {
    path: '/diagram',
    name: 'DiagramView',
    component: DiagramView
  }
]

const router = new VueRouter({
  routes
})

export default router
