'use strict';

import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
import PieChart from "../../../components/PieChart/PieChart";
import List from '../../../components/list/list';

export default {
    name: "TravelReasonOverview",
    components: {
        PulseLoader,
        PieChart,
        List
    },
    data: function () {
        return {
            apiPath: this.$store.state.apiAddress+'stations/withtravelsreasons',
        }
    },
    computed: {
        menuSelect() {
            return this.$store.state.menuSelection;
        },
        stations() {
            if (this.$store.state.apiProxy[this.apiPath]) {
                return this.getStationsWithTravelsReasons(this.$store.state.apiProxy[this.apiPath]);
            } else {
                this.$store.dispatch('fetchApi', this.apiPath);
            }
            return this.$store.state.apiProxyChangeTracker && [];
        },
        currentChart() {
            if (this.menuSelect != null) {
                const uic = this.stations[this.menuSelect].value["STA_CODE"];
                const api = this.$store.state.apiAddress+`stations/${uic}/travelsreasons`;
                if (this.$store.state.apiProxy[api]) {
                    return this.getStationData(this.$store.state.apiProxy[api]);
                } else {
                    this.$store.dispatch('fetchApi', api);
                }
            }
            return this.$store.state.apiProxyChangeTracker && [];
        },
        loading() {
            return this.$store.state.loading;
        }
    },
    methods: {
        getStationsWithTravelsReasons: function (content) {
            let array = [];
            for (const station of content) {
                array.push({
                    "title": station["STA_LIBELLE"],
                    "value": station
                });
            }
            return array;
        },
        getStationData: function (data) {
            const station = data[0];
            const chart = {
                datasets: [{
                    data: [],
                    backgroundColor: [],
                }],
                labels: []
            };
            
            chart["labels"].push("Loisirs, vacances, visite d'un proche ou ami");
            chart["datasets"][0]["data"].push(station["TRA_HOBBY"]);
            chart["datasets"][0]["backgroundColor"].push("#5cb3d5");
            chart["labels"].push("Démarches administratives, médicales ou achat");
            chart["datasets"][0]["data"].push(station["TRA_PROCEDURE"]);
            chart["datasets"][0]["backgroundColor"].push("rgba(165,245,117,0.67)");
            chart["labels"].push("Déplacement professionnel occasionnel");
            chart["datasets"][0]["data"].push(station["TRA_PROFESSIONAL"]);
            chart["datasets"][0]["backgroundColor"].push("rgba(221,152,58,0.68)");
            chart["labels"].push("Déplacement domicile - travail habituel");
            chart["datasets"][0]["data"].push(station["TRA_WORK"]);
            chart["datasets"][0]["backgroundColor"].push("rgba(165,88,211,0.53)");
            chart["labels"].push("Déplacement domicile - étude (y compris stage)");
            chart["datasets"][0]["data"].push(station["TRA_SCHOOL"]);
            chart["datasets"][0]["backgroundColor"].push("rgba(104,104,104,0.19)");
            return chart;
        },
    },
    watch: {
        stations: function (newVal, oldVal) {
            if(oldVal.length == 0 && newVal.length > 0)
            {
                this.$store.commit("updateMenuSelection", 0);
            }
        }
    },
    mounted() {
        if(this.stations.length > 0)
        {
            this.$store.commit("updateMenuSelection", 0);
        }
        else{
            this.$store.commit("updateMenuSelection", null);
        }
    }

}
