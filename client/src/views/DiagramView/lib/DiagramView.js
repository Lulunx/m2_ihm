'use strict';

import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
import BarChart from "../../../components/BarChart/BarChart";
import ListCheck from '../../../components/listCheck/ListCheck';
import {layerGroup} from "leaflet/dist/leaflet-src.esm";

export default {
    name: "DiagramView",
    components: {
        PulseLoader,
        BarChart,
        ListCheck
    },
    data: function () {
        return {
            selected: [],
            apiPath: this.$store.state.apiAddress + 'satisfactions',
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        type: 'category',
                    }]
                }
            },
            color:["#5cb3d5","rgba(165,245,117,0.67)","rgba(221,152,58,0.68)","rgba(165,88,211,0.53)","rgba(104,104,104,0.19)"],
            colorIndex: 0
        }
    },
    computed: {
        currentChart() {
            if (this.menuSelect != null) {
                let toFetch = [];
                let allApi = [];
                for (let indexStation of this.menuSelect) {
                    const uic = this.stations[indexStation].value["STA_CODE"];
                    const api = this.$store.state.apiAddress + `stations/${uic}/satisfaction`;
                    if (!this.$store.state.apiProxy[api]) {
                        toFetch.push(api);
                    }
                    allApi.push(api);
                }
                if (toFetch.length > 0) {
                    for (let api of toFetch) {
                        this.$store.dispatch('fetchApi', api);
                    }
                } else {
                    let data = [];
                    for (let api of allApi) {
                        data.push(this.$store.state.apiProxy[api]);
                    }
                    return this.formatDataForChart(data);
                }
            }
            return this.$store.state.apiProxyChangeTracker && [];
        },
        loading() {
            return this.$store.state.loading;
        },
        stations() {
            if (this.$store.state.apiProxy[this.apiPath]) {
                return this.getStations(this.$store.state.apiProxy[this.apiPath]);
            } else {
                this.$store.dispatch('fetchApi', this.apiPath);
            }
            return this.$store.state.apiProxyChangeTracker && [];
        },
        menuSelect() {
            return this.$store.state.menuListSelection;
        },
    },
    methods: {
        getStations: function (content) {
            let array = [];
            for (const station of content) {
                array.push({
                    "title": station["STA_LIBELLE"],
                    "value": station
                });
            }
            return array;
        },
        formatDataForChart: function (apiData) {
            let chart = {
                labels: ["2017", "2018", "2019"],
                datasets: []
            };
            for (let station of apiData) {
                let dataset = {
                    label: "",
                    data: [],
                    backgroundColor: []
                };
                let labelAdded = false;
                for (let yearData of station) {
                    if (!labelAdded) {
                        dataset["label"] = yearData["STA_LIBELLE"];
                        labelAdded = true;
                    }
                    dataset["data"].push(yearData["SATI_VALUE"]);
                    if (dataset["backgroundColor"].length === 0) {
                        dataset["backgroundColor"].push(this.color[this.colorIndex]);
                    } else {
                        dataset["backgroundColor"].push(dataset["backgroundColor"][0]);
                    }
                }
                chart["datasets"].push(dataset);
                this.colorIndex = (this.colorIndex + 1) % this.color.length;
            }
            return chart;
        },
    },
    mounted() {
        this.$store.commit("updateMenuListSelection", null);
    },
    watch: {
        selected: function (val) {
        }
    }
}