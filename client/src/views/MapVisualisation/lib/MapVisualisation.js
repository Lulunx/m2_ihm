import Map from '../../../components/map/map';
import List from '../../../components/list/list';
import Filtering from '../../../components/filtering/filtering';
import PulseLoader from 'vue-spinner/src/PulseLoader.vue';

export default {
    name: 'MapVisualisation',
    components: {
        Map,
        List,
        Filtering,
        PulseLoader
    },
    data() {
        return {
            menu: [
                {
                    title: 'Liste des gares',
                    subtitle: "Liste des gares du Réseau Ferré National.",
                    api: "stations",
                    filter: null,
                    legends: {
                        green: {
                            marker: "green",
                            text: "Gare"
                        }
                    }
                },
                {
                    title: 'Fréquentation des gares',
                    subtitle: "Fréquentation des gares par an de 2015 à 2018",
                    api: "frequentings",
                    filter: {
                        values: ["Toutes", 2018, 2017, 2016, 2015], name: "Année", selected: "Toutes"
                    },
                    legends: {
                        green: {
                            marker: "green",
                            text: "Fréq. < 100 000"
                        },
                        orange: {
                            marker: "orange",
                            text: "Fréq. > 100 000 et < 1 000 000"
                        },
                        red: {
                            marker: "red",
                            text: "Fréq. > 1 000 000"
                        }
                    }
                },
                {
                    title: 'Satisfaction des clients',
                    subtitle: "Satisfaction des clients par année",
                    api: "satisfactions",
                    filter: {
                        values: ["Toutes", 2019, 2018, 2017], name: "Année", selected: "Toutes"
                    },
                    legends: {
                        green: {
                            marker: "green",
                            text: "Satisfaction > 7.75"
                        },
                        orange: {
                            marker: "orange",
                            text: "Satisfaction > 7.75 et < 7.5"
                        },
                        red: {
                            marker: "red",
                            text: "Satisfcation < 7.5"
                        }
                    }
                },
                {
                    title: 'Propreté des gares',
                    subtitle: "Propreté des gares par année en pourcentage (nombre de fois où la gare est sale/nombre de fois où elle a été controlée)",
                    api: "cleanesses",
                    filter: {
                        values: ["Toutes", 2019, 2018, 2017], name: "Année", selected: "Toutes"
                    },
                    legends: {
                        green: {
                            marker: "green",
                            text: "Propreté > 9.5"
                        },
                        orange: {
                            marker: "orange",
                            text: "Propreté > 9.0 et < 9.5"
                        },
                        red: {
                            marker: "red",
                            text: "Propreté < 9.0"
                        }
                    }
                },

            ]
        }
    },
    computed: {
        filterPath() {
            if (this.menuSelect != null) {
                if (this.menu[this.menuSelect]) {
                    if (this.menu[this.menuSelect].filter) {
                        if (this.menu[this.menuSelect].filter.selected != "Toutes") {
                            return "/year/" + this.menu[this.menuSelect].filter.selected;
                        }
                    }
                }
            }
            return null;
        },
        menuSelect() {
            return this.$store.state.menuSelection;
        },
        apiPath() {
            if (this.menuSelect != null) {
                if (this.menu[this.menuSelect]) {
                    return this.menu[this.menuSelect].api;
                }
            }
            return null
        },
        filter() {
            if (this.menuSelect != null) {
                if (this.menu[this.menuSelect]) {
                    return this.menu[this.menuSelect].filter;
                }
            }
            return null
        },
        dataAPI() {
            if (this.menuSelect != null) {
                if (this.menu[this.menuSelect]) {

                    let api = this.$store.state.apiAddress + this.apiPath;
                    if (this.filterPath != null) {
                        api += this.filterPath
                    }
                    if (this.$store.state.apiProxy[api]) {
                        return this.$store.state.apiProxy[api];
                    } else {
                        this.$store.dispatch('fetchApi', api);
                    }
                }
            }
            return this.$store.state.apiProxyChangeTracker && [];
        },
        loading() {
            return this.$store.state.loading;
        }
    }
}