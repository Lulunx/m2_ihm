'use strict';

import yearSummary from "../../../components/yearSummary/yearSummary";

export default {
    name: "YearlySummariesView",
    components: {
        yearSummary
    },
    mounted() {
        this.$store.commit("updateMenuSelection", null);
    }
}