-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 16 déc. 2019 à 21:04
-- Version du serveur :  10.1.38-MariaDB
-- Version de PHP :  7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ihm`
--

-- --------------------------------------------------------

--
-- Structure de la table `cleaness`
--

CREATE TABLE `cleaness` (
  `CLE_ID` int(11) NOT NULL,
  `STA_CODE` int(11) NOT NULL,
  `CLE_DATE` date NOT NULL,
  `CLE_OBSERVATIONS` int(11) NOT NULL,
  `CLE_IMPROPER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `frequenting`
--

CREATE TABLE `frequenting` (
  `FREQ_ID` int(11) NOT NULL,
  `STA_CODE` int(11) NOT NULL,
  `FREQ_YEAR` int(11) NOT NULL DEFAULT '0',
  `FREQ_VALUE` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `satisfaction`
--

CREATE TABLE `satisfaction` (
  `SATI_ID` int(11) NOT NULL,
  `STA_CODE` int(11) NOT NULL,
  `SATI_YEAR` int(11) NOT NULL,
  `SATI_VALUE` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `station`
--

CREATE TABLE `station` (
  `STA_CODE` int(11) NOT NULL,
  `STA_LIBELLE` varchar(255) NOT NULL,
  `STA_COORD_X` varchar(45) DEFAULT NULL,
  `STA_COORD_Y` varchar(45) DEFAULT NULL,
  `STA_FRET` tinyint(4) NOT NULL DEFAULT '1',
  `STA_TRAVELER` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `travel_type`
--

CREATE TABLE `travel_type` (
  `TRA_ID` int(11) NOT NULL,
  `STA_CODE` int(11) NOT NULL,
  `TRA_YEAR` int(11) NOT NULL,
  `TRA_HOBBY` int(11) NOT NULL DEFAULT '0',
  `TRA_PROCEDURE` int(11) NOT NULL DEFAULT '0',
  `TRA_PROFESSIONAL` int(11) NOT NULL DEFAULT '0',
  `TRA_WORK` int(11) NOT NULL DEFAULT '0',
  `TRA_SCHOOL` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `cleaness`
--
ALTER TABLE `cleaness`
  ADD PRIMARY KEY (`CLE_ID`),
  ADD KEY `fk_CLEANESS_STATION1_idx` (`STA_CODE`);

--
-- Index pour la table `frequenting`
--
ALTER TABLE `frequenting`
  ADD PRIMARY KEY (`FREQ_ID`),
  ADD KEY `fk_FREQUENTING_STATION1_idx` (`STA_CODE`);

--
-- Index pour la table `satisfaction`
--
ALTER TABLE `satisfaction`
  ADD PRIMARY KEY (`SATI_ID`),
  ADD KEY `fk_SATISFACTION_STATION_idx` (`STA_CODE`);

--
-- Index pour la table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`STA_CODE`);

--
-- Index pour la table `travel_type`
--
ALTER TABLE `travel_type`
  ADD PRIMARY KEY (`TRA_ID`),
  ADD KEY `fk_TRAVEL_TYPE_STATION1_idx` (`STA_CODE`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `cleaness`
--
ALTER TABLE `cleaness`
  MODIFY `CLE_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `frequenting`
--
ALTER TABLE `frequenting`
  MODIFY `FREQ_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `satisfaction`
--
ALTER TABLE `satisfaction`
  MODIFY `SATI_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `travel_type`
--
ALTER TABLE `travel_type`
  MODIFY `TRA_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cleaness`
--
ALTER TABLE `cleaness`
  ADD CONSTRAINT `fk_CLEANESS_STATION1` FOREIGN KEY (`STA_CODE`) REFERENCES `station` (`STA_CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `frequenting`
--
ALTER TABLE `frequenting`
  ADD CONSTRAINT `fk_FREQUENTING_STATION1` FOREIGN KEY (`STA_CODE`) REFERENCES `station` (`STA_CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `satisfaction`
--
ALTER TABLE `satisfaction`
  ADD CONSTRAINT `fk_SATISFACTION_STATION` FOREIGN KEY (`STA_CODE`) REFERENCES `station` (`STA_CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `travel_type`
--
ALTER TABLE `travel_type`
  ADD CONSTRAINT `fk_TRAVEL_TYPE_STATION1` FOREIGN KEY (`STA_CODE`) REFERENCES `station` (`STA_CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
