'use strict';

const fs = require('fs');

//On ouvre et parse le fichier json
let rawdata = fs.readFileSync('repartition-des-motifs-de-deplacements-des-voyageurs-et-non-voyageurs-enquetes-e.json');
let motifs = JSON.parse(rawdata);
let toSave = "";
let alreadyDone = {};

let STA_CODE, TRA_YEAR, TRA_HOBBY, TRA_PROCEDURE, TRA_PROFESSIONAL, TRA_WORK, TRA_SCHOOL;
let thisCaseIsOk = false;
//Pour chaque motif de voyage
for (var i = 0; i < motifs.length; i++) {

    thisCaseIsOk = false;
    STA_CODE = motifs[i].fields.uic;
    TRA_YEAR = motifs[i].fields.annee;

    //On check si on a déjà traité cette gare pour l'année en question
    if (!alreadyDone.hasOwnProperty(STA_CODE)) {
        thisCaseIsOk = true;
    }
    else {
        if (!alreadyDone[STA_CODE].includes(TRA_YEAR)) {
            thisCaseIsOk = true;
        }
    }

    //Si pas traité
    if (thisCaseIsOk) {

        TRA_HOBBY = "0";
        TRA_PROCEDURE = "0";
        TRA_PROFESSIONAL = "0";
        TRA_WORK = "0";
        TRA_SCHOOL = "0";

        let nbFound = 0;

        //On cherche les autres motifs de voyage pour la gare, pour l'année
        for (var y = i; y < motifs.length; y++) {

            if (motifs[y].fields.uic == STA_CODE && motifs[y].fields.annee == TRA_YEAR) {
                nbFound++;
                switch (motifs[y].fields.motif_du_deplacement) {
                    case "Loisirs, vacances, visite d'un proche ou ami":
                        TRA_HOBBY = motifs[y].fields.pourcentage;
                        break;
                    case "D\u00e9marches administratives, m\u00e9dicales ou achat":
                        TRA_PROCEDURE = motifs[y].fields.pourcentage;
                        break;
                    case "D\u00e9placement professionnel occasionnel":
                        TRA_PROFESSIONAL = motifs[y].fields.pourcentage;
                        break;
                    case "D\u00e9placement domicile - travail habituel":
                        TRA_WORK = motifs[y].fields.pourcentage;
                        break;
                    case "D\u00e9placement domicile - \u00e9tude (y compris stage)":
                        TRA_SCHOOL = motifs[y].fields.pourcentage;
                        break;
                    default:
                        nbFound--;
                        break;
                }
                if (nbFound == 5) {
                    break;
                }
            }
        }

        //On indique qu'on a traité cette année pour cette gare
        if (alreadyDone.hasOwnProperty(STA_CODE)) {
            alreadyDone[STA_CODE].push(TRA_YEAR)
        }
        else {
            alreadyDone[STA_CODE] = [TRA_YEAR];
        }

        //On ajoute l'insertion pour les motifs de la gare en question
        toSave += '\n INSERT INTO TRAVEL_TYPE (STA_CODE, TRA_YEAR, TRA_HOBBY, TRA_PROCEDURE, TRA_PROFESSIONAL, TRA_WORK, TRA_SCHOOL) VALUES (87' +
            STA_CODE + ', ' + TRA_YEAR + ', ' + TRA_HOBBY + ', ' + TRA_PROCEDURE + ', ' + TRA_PROFESSIONAL + ', ' + TRA_WORK + ', ' + TRA_SCHOOL + ' );'
    }
}

fs.writeFile("travel_reason.sql", toSave, function(){
});