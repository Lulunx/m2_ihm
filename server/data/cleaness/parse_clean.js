'use strict';

const fs = require('fs');

//On ouvre le fichier json et on parse
let rawdata = fs.readFileSync('proprete-en-gare.json');
let cleans = JSON.parse(rawdata);
let toSave = "";

//Pour chaque objet json
for (var i = 0; i < cleans.length; i++) {
    //On ajoute l'insertion pour la propreté en question
    toSave += '\n INSERT INTO cleaness (STA_CODE, CLE_DATE, CLE_OBSERVATIONS, CLE_IMPROPER) VALUES (' +
        cleans[i].fields.uic.substr(2) + ', "' + cleans[i].fields.mois + '-01", ' +
        cleans[i].fields.nombre_d_observations + ', ' + cleans[i].fields.nombre_de_non_conformites + ');'
}

//On sauvegarde le fichier
fs.writeFile("cleaness.sql", toSave, function () {
});

