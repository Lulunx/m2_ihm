'use strict';

const fs = require('fs');

//On ouvre le fichier json et on parse
let rawdata = fs.readFileSync('frequentation-gares.json');
let freq = JSON.parse(rawdata);
let toSave = "";

//Pour chaque objet json
for (var i = 0; i < freq.length; i++) {
    //On ajoute l'insertion pour la frequentation en question
    toSave += '\n INSERT INTO frequenting (STA_CODE, FREQ_YEAR, FREQ_VALUE) VALUES (' +freq[i].fields.code_uic_complet + ', 2015, ' + freq[i].fields.total_voyageurs_2015 +');';
    toSave += '\n INSERT INTO frequenting (STA_CODE, FREQ_YEAR, FREQ_VALUE) VALUES (' +freq[i].fields.code_uic_complet + ', 2016, ' + freq[i].fields.total_voyageurs_2016 +');';
    toSave += '\n INSERT INTO frequenting (STA_CODE, FREQ_YEAR, FREQ_VALUE) VALUES (' +freq[i].fields.code_uic_complet + ', 2017, ' + freq[i].fields.totalvoyageurs2017 +');';
    toSave += '\n INSERT INTO frequenting (STA_CODE, FREQ_YEAR, FREQ_VALUE) VALUES (' +freq[i].fields.code_uic_complet + ', 2018, ' + freq[i].fields.total_voyageurs_2018 +');';
}

//On sauvegarde le fichier
fs.writeFile("frequenting.sql", toSave, function () {
});