'use strict';

const fs = require('fs');

//On ouvre le json et on parse
let rawdata = fs.readFileSync('liste-des-gares.json');
let gares = JSON.parse(rawdata);
let toSave = "";
let alreadyDone = [];

//Pour chaque gare
for (var i = 0; i < gares.length; i++) {
    //Si on a pas déjà traité l'uic en question ( uic = identifiant de la gare ) (OUI OUI Y'A DES DOUBLONS MERCI SNCF)
    if (!alreadyDone.includes(gares[i].fields.code_uic)) {

        //On récupère les coordonnées géo (elles ne sont pas toujours au même endroit) (OUI OUI TRES INTELLIGENT MERCI SNCF)
        if ('c_geo' in gares[i].fields || 'coordonnees_geo' in gares[i].fields) {
            if ('c_geo' in gares[i].fields) {
                gares[i].x = gares[i].fields.c_geo[0];
                gares[i].y = gares[i].fields.c_geo[1];
            }
            else {
                gares[i].x = gares[i].fields.coordonnees_geo[0];
                gares[i].y = gares[i].fields.coordonnees_geo[1];
            }

            //On ajoute l'insertion pour la gare en question
            gares[i].text = gares[i].fields.libelle;
            gares[i].fret = gares[i].fields.fret == "O" ? 1 : 0;
            gares[i].voyageurs = gares[i].fields.voyageurs == "O" ? 1 : 0;
            toSave += '\n INSERT INTO station (STA_CODE, STA_LIBELLE, STA_COORD_X, STA_COORD_Y, STA_FRET, STA_TRAVELER) VALUES ("' + gares[i].fields.code_uic + '", "' + gares[i].text + '", "' + gares[i].x + '", "' + gares[i].y + '", ' + gares[i].fret + ', ' + gares[i].voyageurs + ' );';
            alreadyDone.push(gares[i].fields.code_uic);
        }
    }
}
fs.writeFile("stations.sql", toSave, function () {
});