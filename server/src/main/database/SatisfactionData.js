'use strict';

class SatisfactionData {

    constructor(dbconn) {
        this._db = dbconn;
    }


    get dbconn() {
        return this._db;
    }

    set dbconn(value) {
        this._db = value;
    }

    getSatisfactionByYear(year) {
        return this.dbconn.makeQuery(`
        SELECT 
            AVG(SATI_VALUE) as SATI_VALUE,
            STA_LIBELLE,
            STA_CODE,
            STA_COORD_X,
            STA_COORD_Y
        FROM satisfaction 
        join station using(STA_CODE) 
        where SATI_YEAR = ${year}
        and SATI_VALUE != -1
        group by(STA_CODE);
        `);
    }

    getSatisfaction() {
        return this.dbconn.makeQuery(`
        SELECT 
            AVG(SATI_VALUE) as SATI_VALUE,
            STA_LIBELLE,
            STA_CODE,
            STA_COORD_X,
            STA_COORD_Y
        FROM satisfaction 
        join station using(STA_CODE) 
        where SATI_VALUE != -1
        group by(STA_CODE);
    `);
    }

    
}

module.exports = SatisfactionData;