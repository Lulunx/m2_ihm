'use strict';

class CleanessData {

    constructor(dbconn) {
        this._db = dbconn;
    }


    get dbconn() {
        return this._db;
    }

    set dbconn(value) {
        this._db = value;
    }

    getCleanessByYear(year) {
        return this.dbconn.makeQuery(`
        SELECT 
            SUM(CLE_OBSERVATIONS) as CLE_OBSERVATIONS,
            SUM(CLE_IMPROPER) as CLE_IMPROPER,
            STA_LIBELLE,
            STA_CODE,
            STA_COORD_X,
            STA_COORD_Y
        FROM cleaness 
        join station using(STA_CODE) 
        where CLE_DATE between "`+year+`-01-01" and "`+year+`-12-31"
        group by(STA_CODE);
        `);
    }

    getCleaness() {
        return this.dbconn.makeQuery(`
        SELECT SUM(CLE_OBSERVATIONS) as CLE_OBSERVATIONS,
            SUM(CLE_IMPROPER) as CLE_IMPROPER,
            STA_LIBELLE,
            STA_CODE,
            STA_COORD_X,
            STA_COORD_Y FROM cleaness join station using(STA_CODE) group by STA_CODE;
        `);
    }

    
}

module.exports = CleanessData;