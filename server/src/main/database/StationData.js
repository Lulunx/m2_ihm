'use strict';

class StationData {

    constructor(dbconn) {
        this._db = dbconn;
    }


    get dbconn() {
        return this._db;
    }

    set dbconn(value) {
        this._db = value;
    }

    getStationsWithTravelsReasons() {
        return this.dbconn.makeQuery(`
            select travel_type.STA_CODE, STA_LIBELLE
            from travel_type,
                 station
            where travel_type.STA_CODE = station.STA_CODE;`);
    }

    getStationTravelReasons(uic) {
        return this.dbconn.makeQuery(`
            select * from travel_type where STA_CODE = ${uic};
        `);
    }

    getStationCleaness(uid) {
        return this.dbconn.makeQuery(`
            SELECT *
            FROM cleaness
            where STA_CODE = ` + uid + `;
        `);
    }

    getStationSatisfaction(uid) {
        return this.dbconn.makeQuery(`
            SELECT satisfaction.STA_CODE, SATI_YEAR, SATI_VALUE, STA_LIBELLE
            FROM satisfaction
            JOIN station ON station.STA_CODE = satisfaction.STA_CODE
            WHERE satisfaction.STA_CODE = ${uid};
        `);
    }

    getStations() {
        return this.dbconn.makeQuery(`
            select *
            from station;
        `);
    }

    getStationsStats(year) {
        return this.dbconn.makeQuery(`
            SELECT station.STA_CODE,
                   STA_LIBELLE,
                   SATI_VALUE,
                   FREQ_VALUE,
                   ((1 - (SUM(CLE_IMPROPER) / SUM(CLE_OBSERVATIONS))) * 10) as CLE_CLEANESS
            FROM ihm.satisfaction
                     JOIN station on station.STA_CODE = satisfaction.STA_CODE
                     JOIN cleaness on station.STA_CODE = cleaness.STA_CODE
                     JOIN frequenting on station.STA_CODE = frequenting.STA_CODE
            where (CLE_DATE between "${year}-01-01" and "${year}-12-31")
              and SATI_YEAR = ${year}
              and SATI_VALUE >= 0
            group by(STA_LIBELLE)
            order by SATI_VALUE;`);
    }
}

module.exports = StationData;