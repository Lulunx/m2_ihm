'use strict';

class FrequentingData {

    constructor(dbconn) {
        this._db = dbconn;
    }


    get dbconn() {
        return this._db;
    }

    set dbconn(value) {
        this._db = value;
    }

    getFrequentingByYear(year) {
        return this.dbconn.makeQuery(`
        SELECT 
            AVG(FREQ_VALUE) as FREQ_VALUE,
            STA_LIBELLE,
            STA_CODE,
            STA_COORD_X,
            STA_COORD_Y
        FROM frequenting 
        join station using(STA_CODE) 
        where FREQ_YEAR = `+year+`
        group by(STA_CODE);
        `);
    }

    getFrequenting() {
        return this.dbconn.makeQuery(`
        SELECT 
        AVG(FREQ_VALUE) as FREQ_VALUE,
        STA_LIBELLE,
        STA_CODE,
        STA_COORD_X,
        STA_COORD_Y
    FROM frequenting 
    join station using(STA_CODE) 
    group by(STA_CODE);
        `);
    }

    
}

module.exports = FrequentingData;