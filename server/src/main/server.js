'use strict';
// Import swagger
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Import Module Express
const Express = require('express');
const BodyParser = require('body-parser');
const cors = require('cors');

// Import settings
const Settings = require('../../settings');

const rooter = require('./controllers/rooter');

// Paramètres du serveur
const hostname = Settings.express.hostname;
const port = Settings.express.port;
const app = Express();
app.use(cors());
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({extended: true}));
app.use(rooter());

// swagger settings
const options = {
    definition: {
        openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
        info: {
            title: 'IHM API', // Title (required)
            version: '1.0.0', // Version (required)
            contact: {
                name: 'Lucas Gancel & Nicolas Hoffmann'
            }
        },
    },
    tags: [
        {
            name: 'CRUD operations'
        }
    ],
    // Path to the API docs
    apis: [
        './src/main/controllers/resources/HelloWorldController.js',
        './src/main/controllers/resources/StationController.js',
        './src/main/controllers/resources/CleanessController.js',
        './src/main/controllers/resources/FrequentingController.js',
        './src/main/controllers/resources/SatisfactionController.js',
        './src/main/utils/HttpResponse.js'
    ]
};

const swaggerSpec = swaggerJSDoc(options);



// swagger url
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Lance le serveur
app.listen(port, () => {
});