'use strict';
const express = require('express');
const StationController = require('./resources/StationController');
const CleanessController = require('./resources/CleanessController');
const FrequentingController = require('./resources/FrequentingController');
const SatisfactionController = require('./resources/SatisfactionController');
const rooter = function () {
    const router = express.Router();
    router.use('/stations', StationController());
    router.use('/cleanesses', CleanessController());
    router.use('/frequentings', FrequentingController());
    router.use('/satisfactions', SatisfactionController());
    return router;
};

module.exports = rooter;
