'use strict';
const express = require('express');
const HttpResponse = require('../../utils/HttpResponse');
const db = require('../../database/conn');
const FrequentingController = require('../../database/FrequentingData');

const CleanessController = function () {
    const router = express.Router();

    const frequentingData = new FrequentingController(db);

    /**
     * @swagger
     * /frequentings:
     *  get:
     *      description: Retrieves all frequentings of the database group by station
     *      responses:
     *          '200':
     *              description: frequentings data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('', (req, res) => {
        frequentingData.getFrequenting().then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /frequentings/year/{year}:
     *  get:
     *      description: Retrieves all frequentings of the database group by station for a year
     *      parameters:
     *          - in: path
     *            name: year
     *            schema:
     *              type: string
     *              example: 2018
     *            required: true
     *            description: Year you want data for
     *      responses:
     *          '200':
     *              description: Station data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/year/:year', (req, res) => {
        frequentingData.getFrequentingByYear(req.params.year).then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });
    

    
    return router;
};

module.exports = CleanessController;