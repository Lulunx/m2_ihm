'use strict';
const express = require('express');
const HttpResponse = require('../../utils/HttpResponse');
const db = require('../../database/conn');
const StationData = require('../../database/StationData');

const StationController = function () {
    const router = express.Router();

    const stationData = new StationData(db);

    /**
     * @swagger
     * /stations:
     *  get:
     *      description: Retrieves all stations of the database
     *      responses:
     *          '200':
     *              description: Stations data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('', (req, res) => {
        stationData.getStations().then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /stations/{uic}/travelsreasons:
     *  get:
     *      description: Retrieves travels reasons of a station
     *      parameters:
     *          - in: path
     *            name: uic
     *            schema:
     *              type: string
     *              example: 87444000
     *            required: true
     *            description: Unique identifier of a station
     *      responses:
     *          '200':
     *              description: Station data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/:uic/travelsreasons', (req, res) => {
        stationData.getStationTravelReasons(req.params.uic).then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /stations/{uic}/cleaness:
     *  get:
     *      description: Retrieves cleanesses of a station
     *      parameters:
     *          - in: path
     *            name: uic
     *            schema:
     *              type: string
     *              example: 87444000
     *            required: true
     *            description: Unique identifier of a station
     *      responses:
     *          '200':
     *              description: Station data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/:uic/cleaness', (req, res) => {
        stationData.getStationCleaness(req.params.uic).then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });
/**
     * @swagger
     * /stations/{uic}/satisfaction:
     *  get:
     *      description: Retrieves satisfaction of a station
     *      parameters:
     *          - in: path
     *            name: uic
     *            schema:
     *              type: string
     *              example: 87444000
     *            required: true
     *            description: Unique identifier of a station
     *      responses:
     *          '200':
     *              description: Station data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/:uic/satisfaction', (req, res) => {
        stationData.getStationSatisfaction(req.params.uic).then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /stations/withtravelsreasons:
     *  get:
     *      description: Retrieves all stations that have data for travel reasons
     *      responses:
     *          '200':
     *              description: Station data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/withtravelsreasons', (req, res) => {
        stationData.getStationsWithTravelsReasons().then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /stations/stats/{year}:
     *  get:
     *      description: Retrieves all stats for a year
     *      parameters:
     *          - in: path
     *            name: year
     *            schema:
     *              type: string
     *              example: 2018
     *            required: true
     *            description: year (4 digits)
     *      responses:
     *          '200':
     *              description: Stations data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/stats/:year', (req, res) => {
        stationData.getStationsStats(req.params.year).then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });


    return router;
};

module.exports = StationController;