'use strict';
const express = require('express');
const HttpResponse = require('../../utils/HttpResponse');
const db = require('../../database/conn');
const SatisfactionController = require('../../database/SatisfactionData');

const CleanessController = function () {
    const router = express.Router();

    const satisfactionData = new SatisfactionController(db);

    /**
     * @swagger
     * /satisfactions:
     *  get:
     *      description: Retrieves all satisfactions of the database group by station
     *      responses:
     *          '200':
     *              description: satisfactions data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('', (req, res) => {
        satisfactionData.getSatisfaction().then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });

    /**
     * @swagger
     * /satisfactions/year/{year}:
     *  get:
     *      description: Retrieves all satisfactions of the database group by station for a year
     *      parameters:
     *          - in: path
     *            name: year
     *            schema:
     *              type: string
     *              example: 2018
     *            required: true
     *            description: Year you want data for
     *      responses:
     *          '200':
     *              description: Station data retrieved
     *              content:
     *                  application/json:
     *                      schema:
     *                          $ref: '#/components/schemas/HttpResponse'
     */
    router.get('/year/:year', (req, res) => {
        satisfactionData.getSatisfactionByYear(req.params.year).then( (rows) => {
            res.status(200);
            let response = new HttpResponse(res.statusCode);
            response.message = 'OK';
            response.content = rows;
            res.json(response.toJSON());
        });
    });
    

    
    return router;
};

module.exports = CleanessController;