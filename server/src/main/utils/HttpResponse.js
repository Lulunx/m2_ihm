'use strict';

/**
 * @swagger
 * components:
 *  schemas:
 *      HttpResponse:
 *          description: Basic response format
 *          type: object
 *          properties:
 *              status:
 *                  type: string
 *              message:
 *                  type: string
 *              content:
 *                  type: object
 *          required:
 *              - status
 */

class HttpResponse {
    constructor(status, message, object) {
        this.status = status;
        if (message) this.message = message;
        if (object) this.content = object;
    }

    toJSON() {
        let res = {"status": this.status};
        if (this.message) res["message"] = this.message;
        if (this.content) res["content"] = this.content;
        return res;
    }
}

module.exports = HttpResponse;