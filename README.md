# Projet IHM


## Execution du client
    cd client
    npm install
    npm run server

## Execution du serveur
    cd server
    npm install
    npm start

## Creation de la base de données
    Executer le script /server/data/mld.sql
    Executer le script /server/data/stations/stations.sql
    Executer le script /server/data/cleaness/cleaness.sql
    Executer le script /server/data/frequenting/frequenting.sql
    Executer le script /server/data/satisfaction/satisfaction.sql
    Executer le script /server/data/travel_reason/travel_reason.sql

Il peut être nécessaire de changer les adresses de l'api pour le client et de la base de données pour l'api :
L'adresse de l'api est dans le store /client/src/store/index.js
L'adresse de la base de données pour l'api /server/settings.json (settings.json.dist a transformer en settings.json et à parameter )
